#include <stdio.h>
#include <stdlib.h>
#define ON 1
#define OFF 0
void add_node(void);
void print_all_nodes(void);
void delete_all_nodes(void);
void search_node(void);
void delete_particular_node(void);
typedef struct LinkList
{
    int EleNumber;
    char EleName[10];
    struct LinkList * next;
}LinkList;

LinkList *First=NULL,*temp=NULL,*traverse=NULL;
int NumOfNodes=0,NodeCount=0,tempvar;

int main(int argc, char * argv[])
{
    NumOfNodes = 0;
    int loop = ON, UserInput;
    while(loop == ON)
    {
	    system("clear");
	    fflush(stdin);
        printf("Press 1 to create new node.\nPress 2 to search for an existing node.\nPress 3 to delete an exiting node.\nPress 4 to print all available nodes\nEnter any option to proceed: ");
		scanf("%d",&UserInput);
		getchar();
		if (UserInput == 0)
		{
		    printf("Exiting!!!!!!!\n");
			delete_all_nodes(); //Call function to delete all the available nodes.
		    loop = OFF;      //Stop the function and exit!!!!
		}
		else if(UserInput == 1)
		{
		    add_node();       //Call function to add node
		}
		else if (UserInput == 2)
		{
		    search_node();    //Call function to search for node
		}
		else if (UserInput == 3)
		{
		    delete_particular_node();//Delete Existing node
		}
		else if (UserInput == 4)
		{
		    print_all_nodes();//Print all the available nodes
		}
		else
		{
		    printf("Invalid Input!!! Try Again....");
			scanf("%d",&tempvar);
			continue;
		}
    }
}
void delete_particular_node(void)
{
	int nodenum,nodefound=0;
	LinkList *localtemp,*localtemp_prev;
	printf("Enter node number you want to delete: ");
	scanf("%d",&nodenum);
	temp = First;
	while(temp != NULL)	
	{
		if (First->EleNumber == nodenum)
		{
			nodefound = 1;
			localtemp = First;
			First = First->next;
			free(localtemp);
			temp = First;
			NodeCount--;
			printf("Deleted!!!\n");	
		    printf("Press any key to continue....");
	        scanf("%d",&tempvar);	
		}
		else if (temp->EleNumber == nodenum)
		{
			nodefound = 1;
			localtemp = temp;
			temp = First;
			localtemp_prev = First;
			while (temp != NULL)
			{
				if (temp->EleNumber != nodenum)
				{
					localtemp_prev = temp;
					temp = temp->next;
				}
				else
				{
					break;					
				}
			}
			localtemp_prev->next = localtemp->next;
			free(localtemp);
			NodeCount--;
			printf("Deleted!!!\n");	
		    printf("Press any key to continue....");
	        scanf("%d",&tempvar);	
		}
		else
		{
			temp = temp->next;
			nodefound = 0;
		}
	}
	if (nodefound == 0)
	{
		printf("Entered node not found!! Try Again\n");	
		printf("Press any key to continue....");
	    scanf("%d",&tempvar);	
	}
}
void add_node(void)
{   
    if (NodeCount == 0)
	{
		NumOfNodes++;
        First = (LinkList *)malloc(sizeof(LinkList));
		First->EleNumber = NumOfNodes;
	    printf("Enter Node Name: ");
	    scanf("%[^\n]%*c",First->EleName);
		traverse = First;
	}
	else
	{
        NumOfNodes++;
	    temp = (LinkList *)malloc(sizeof(LinkList));
	    temp->EleNumber = NumOfNodes;
	    printf("Enter Node Name: ");
	    scanf("%[^\n]%*c",temp->EleName);
	    temp->next = NULL;
		traverse->next = temp;
		traverse = traverse->next;
	}
	NodeCount++;
}
void print_all_nodes(void)
{
    temp = First;
	while(temp != NULL)
	{
	    printf("Node Number: %d\n", temp->EleNumber);
	    printf("Node Name: %s\n", temp->EleName);
		temp = temp->next;
	}
	printf("Press any key to continue....");
	scanf("%d",&tempvar);
}
void delete_all_nodes(void)
{
    temp = First;
	traverse = First;
	while(temp != NULL)
	{
		traverse = traverse->next;
		free(temp);
	    temp = traverse;
		NodeCount--;
	}
	printf("Pending number of nodes: %d\n",NodeCount);
	printf("Press any key to continue....");
	scanf("%d",&tempvar);
	system("clear");
}
void search_node(void)
{
    int nodenum,found = 0;
	printf("Enter node number you want to search: ");
	scanf("%d",&nodenum);
	temp = First;
	while(temp != NULL)
	{
	    if (temp->EleNumber == nodenum)
		{
		    printf("Node Found!!!!\n");
			printf("Name of node is: %s\n",temp->EleName);
	        printf("Press any key to continue....");
	        scanf("%d",&tempvar);
			found = 1;
			break;
		}
		else
		{
		    temp = temp->next;
		}
	}
	if (found == 0)
	{
	    printf("Entered node not found!!!\n");
	    printf("Press any key to continue....");
	    scanf("%d",&tempvar);
	}
}
